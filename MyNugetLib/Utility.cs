﻿using MyNugetLib.Models;
using System.Collections.Generic;

namespace MyNugetLib
{
    public class Utility
    {
        public string Message()
        {
            return "Hello World";
        }

        public List<CarModel> GetCurrentCarList()
        {
            return new List<CarModel>
            {
                new CarModel { Name = "Audi" },
                new CarModel { Name = "Ford" },
                new CarModel { Name = "Passat" }
            };
        }
    }
}
